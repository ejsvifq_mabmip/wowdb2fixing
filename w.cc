#include<fstream>
#include<unordered_map>
#include<sstream>
#include<tuple>
#include<algorithm>
#include<iostream>

int main()
{
	std::ifstream texturefiledata("texturefiledata.csv",std::ifstream::binary);
	std::unordered_map<std::size_t,std::size_t> map;
	for(std::size_t a,b;texturefiledata>>a>>b;)
		map[b]=a;
	std::ifstream fin("dump.txt",std::ifstream::binary);
	std::ifstream listfile("listfile.csv",std::ifstream::binary);
	std::unordered_map<std::size_t,std::string> listmap;
	for(std::string str;std::getline(listfile,str);)
	{
		std::istringstream iss(std::move(str));
		std::size_t a;
		iss>>a;
		int ch(iss.get());
		std::string s;
		for(;(ch=iss.get())!=EOF;s.push_back(ch));
		listmap[a]=s;
	}
	std::vector<std::tuple<std::size_t,std::size_t,std::size_t,std::size_t,std::string>> vec;
	for(std::size_t race,sex,v;fin>>race>>sex>>v;)
	{
		std::size_t val(map.at(v));
		vec.emplace_back(race,sex,v,val,listmap.at(val));
	}
	sort(vec.begin(),vec.end());
	std::ofstream fout("missing_file_ids_for_SDs_related_to_HD.txt",std::ofstream::binary);
	fout<<"RaceID SexID HDMaterialResourcesID HDFileDataID HDFilePath\n";
	for(auto const & e : vec)
	{
		fout<<std::get<0>(e)<<" "<<std::get<1>(e)<<" "<<std::get<2>(e)<<" "<<std::get<3>(e)<<" "<<std::get<4>(e)<<"\n";
	}
}